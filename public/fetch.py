import requests
import os, fnmatch, subprocess
import getpass, re, csv, json
from pprint import pprint as pprint

def geocode(city, country):
    url = "http://nominatim.openstreetmap.org/search?format=json&q="
    address = city + "+" + country
    address = address.replace("near ", "")
    address = address.replace(" ", "+")
    print(address)
    r = requests.get(url+address)
    r = r.json()
    if r == []:
        address = country
        address = address.replace("near ", "")
        address = address.replace(" ", "+")
        r = requests.get(url+address)
        r = r.json()
    if r == []:
        return (0, 0)
    return (r[0]['lat'], r[0]['lon'])


def fetch():
    r = requests.get('http://api.nobelprize.org/v1/laureate.json')
    r = r.json()
    laureates = []
    for laureate in r['laureates']:
        if laureate['gender'] == 'org':
            continue
        if (laureate.has_key('bornCountry') == False or laureate['bornCountry'] == ""):
            continue
        if ((laureate.has_key('bornCity') == False) or laureate['bornCity'] == ""):
            continue
        if (laureate.has_key('born') == False or laureate['born'][:4] == "0000"):
            continue
        if int(laureate['born'][5:7]) == 0 or int(laureate['born'][8:10]) == 0:
            continue
        if "(now" in laureate['bornCity']:
            laureate['bornCity'] = laureate['bornCity'][laureate['bornCity'].find("now ")+4:len(laureate['bornCity'])-1] 
        if "(now" in laureate['bornCountry']:
            laureate['bornCountry'] = laureate['bornCountry'][laureate['bornCountry'].find("now ")+4:len(laureate['bornCountry'])-1] 
        lat, lon = geocode(laureate['bornCity'], laureate['bornCountry'])
        print(lat, lon)
        laureates.append({
            "id" : int(laureate['id']),
            "bornCity" : laureate['bornCity'],
            "bornCountry" : laureate['bornCountry'],
            "bornYear" : int(laureate['born'][:4]),
            "bornMonth" : int(laureate['born'][5:7]),
            "bornDay" : int(laureate['born'][8:10]),
            "agePrize" : int(laureate['prizes'][0]['year']) - int(laureate['born'][:4]),
            "category" : (laureate['prizes'][0]['category']),
            "gender" : laureate['gender'],
            "lat" : lat,
            "lon" : lon,
            "dist" : 0
            })
    return laureates

if __name__ == '__main__':
    laureates = fetch()
    with open('./parsedLaureates.json', 'w+') as outfile:
            outfile.write(json.dumps(laureates, indent=4))